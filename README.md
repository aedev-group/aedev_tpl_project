<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.23 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# tpl_project 0.3.32

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_project/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_project)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_project/release0.3.31?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_project/-/tree/release0.3.31)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_tpl_project)](
    https://pypi.org/project/aedev-tpl-project/#history)

>aedev_tpl_project package 0.3.32.

[![Coverage](https://aedev-group.gitlab.io/aedev_tpl_project/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_project/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_tpl_project/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_project/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_tpl_project/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_project/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_tpl_project)](
    https://gitlab.com/aedev-group/aedev_tpl_project/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_tpl_project)](
    https://gitlab.com/aedev-group/aedev_tpl_project/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_tpl_project)](
    https://gitlab.com/aedev-group/aedev_tpl_project/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_tpl_project)](
    https://pypi.org/project/aedev-tpl-project/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_tpl_project)](
    https://gitlab.com/aedev-group/aedev_tpl_project/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_tpl_project)](
    https://libraries.io/pypi/aedev-tpl-project)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_tpl_project)](
    https://pypi.org/project/aedev-tpl-project/#files)


## installation


execute the following command to install the
aedev.tpl_project package
in the currently active virtual environment:
 
```shell script
pip install aedev-tpl-project
```

if you want to contribute to this portion then first fork
[the aedev_tpl_project repository at GitLab](
https://gitlab.com/aedev-group/aedev_tpl_project "aedev.tpl_project code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_tpl_project):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_tpl_project/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.tpl_project.html
"aedev_tpl_project documentation").
