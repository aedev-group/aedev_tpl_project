anybadge
coverage-badge
aedev_git_repo_manager      # including Pillow, ae_base, ae_files, ae_paths for img/loc/snd resources tests
flake8
mypy
pylint
pytest
pytest-cov
pytest-django
typing
types-setuptools            # for mypy
wheel
twine
